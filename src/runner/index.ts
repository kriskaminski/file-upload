import * as path from "path";
import { NodeBlock, assert, filter } from "tripetto-runner-foundation";
import "./condition";

export interface IFileUpload {
    /** Maximum file size in Mb. */
    limit?: number;

    /** Comma separated list of allowed file extensions. */
    extensions?: string;
}

export interface IFileUploadService {
    /** Fetch an attachment from the store. */
    readonly get: (file: string) => Promise<Blob>;

    /** Put an attachment in the store. */
    readonly put: (
        file: File,
        onProgress?: (percentage: number) => void
    ) => Promise<string>;

    /** Delete an attachment from the store. */
    readonly delete: (file: string) => Promise<void>;
}

export abstract class FileUpload extends NodeBlock<IFileUpload> {
    private cache: string | undefined;
    readonly fileSlot = assert(this.valueOf<string>("file"));
    readonly required = this.fileSlot.slot.required || false;

    get isUploading() {
        return this.fileSlot.isAwaiting;
    }

    get isImage() {
        return (
            this.fileSlot.hasValue &&
            this.hasImageExtension(this.fileSlot.value)
        );
    }

    get limit() {
        return this.props.limit && this.props.limit > 0 ? this.props.limit : 10;
    }

    get allowedExtensions() {
        return filter(
            (this.props.extensions &&
                this.props.extensions
                    .toLowerCase()
                    .replace(" ", "")
                    .split(",")) ||
                [],
            (extension) => extension.length >= 2 && extension.charAt(0) === "."
        );
    }

    private hasImageExtension(fileName: string): boolean {
        return (
            (fileName &&
                [".jpg", ".jpeg", ".png", ".gif"].indexOf(
                    path.extname(fileName).toLowerCase()
                ) > -1) ||
            false
        );
    }

    private hasValidFileSize(file: File): boolean {
        if (this.props.limit) {
            const maximumFileBytes = this.props.limit * 1024 * 1024;

            return file.size <= maximumFileBytes;
        }

        return true;
    }

    private hasValidFileExtension(file: File): boolean {
        const fileExtension = path.extname(file.name).toLowerCase();
        const allowedExtensions = this.allowedExtensions;

        if (allowedExtensions.length > 0) {
            return allowedExtensions.indexOf(fileExtension) > -1;
        }

        return true;
    }

    private convertToBase64(
        blob: Blob,
        done: (data: string) => void,
        progress?: (percentage: number) => void
    ): void {
        const reader = new FileReader();

        if (progress) {
            reader.onprogress = (event) =>
                progress((event.loaded / event.total) * 100);
        }

        reader.onload = () => {
            done(reader.result as string);
        };

        reader.readAsDataURL(blob);
    }

    upload(
        files: FileList,
        service?: IFileUploadService,
        onProgress?: (percent: number) => void
    ): Promise<void> {
        this.cache = undefined;

        this.fileSlot.clear();

        return new Promise(
            (
                resolve: () => void,
                reject: (
                    error:
                        | "invalid-amount"
                        | "invalid-extension"
                        | "invalid-size"
                        | string
                ) => void
            ) => {
                if (files.length !== 1) {
                    return reject("invalid-amount");
                }

                const file = files[0];

                if (!this.hasValidFileExtension(file)) {
                    return reject("invalid-extension");
                }

                if (!this.hasValidFileSize(file)) {
                    return reject("invalid-size");
                }

                this.fileSlot.await();

                if (service) {
                    service
                        .put(file, onProgress)
                        .then((id) => {
                            this.fileSlot.set(file.name, id);

                            if (this.isImage) {
                                this.convertToBase64(file, (data) => {
                                    this.cache = data;

                                    resolve();
                                });
                            } else {
                                resolve();
                            }
                        })
                        .catch((error) => {
                            this.fileSlot.clear();

                            reject(error);
                        });
                } else {
                    this.convertToBase64(
                        file,
                        (data) => {
                            this.fileSlot.set(file.name, data);

                            resolve();
                        },
                        onProgress
                    );
                }
            }
        );
    }

    download(service?: IFileUploadService): Promise<string> {
        return new Promise(
            (resolve: (data: string) => void, reject: () => void) => {
                if (this.cache) {
                    resolve(this.cache);
                } else if (this.fileSlot.reference) {
                    if (service) {
                        service
                            .get(this.fileSlot.reference)
                            .then((blob) =>
                                this.convertToBase64(blob, (data) =>
                                    resolve(data)
                                )
                            )
                            .catch(() => {
                                this.fileSlot.clear();

                                reject();
                            });
                    } else {
                        resolve(this.fileSlot.reference);
                    }
                } else {
                    reject();
                }
            }
        );
    }

    delete(service?: IFileUploadService): Promise<void> {
        return new Promise((resolve: () => void, reject: () => void) => {
            if (this.fileSlot.reference && service) {
                this.fileSlot.await();

                service
                    .delete(this.fileSlot.reference)
                    .then(() => {
                        this.cache = undefined;
                        this.fileSlot.clear();

                        resolve();
                    })
                    .catch(() => {
                        this.fileSlot.cancelAwait();

                        reject();
                    });
            } else {
                this.cache = undefined;
                this.fileSlot.clear();

                resolve();
            }
        });
    }
}
