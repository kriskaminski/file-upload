/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Forms,
    NodeBlock,
    Slots,
    conditions,
    definition,
    each,
    editor,
    isNumberFinite,
    isString,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { FileUploadCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_UPLOADED from "../../assets/icon-uploaded.svg";
import ICON_NOT_UPLOADED from "../../assets/icon-not-uploaded.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    alias: "file-upload",
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:file-upload", "File upload");
    },
})
export class FileUpload extends NodeBlock {
    public fileSlot!: Slots.String;

    @definition
    limit?: number;

    @definition
    extensions?: string;

    @slots
    defineSlots(): void {
        this.fileSlot = this.slots.static({
            type: Slots.String,
            reference: "file",
            label: pgettext("block:file-upload", "File upload"),
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.explanation();

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:file-upload", "File size"),
            form: {
                title: pgettext("block:file-upload", "Maximum file size"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "limit", undefined, 1)
                    )
                        .min(1)
                        .suffix(" Mb"),
                ],
            },
            activated: isNumberFinite(this.limit),
        });
        this.editor.option({
            name: pgettext("block:file-upload", "File type"),
            form: {
                title: pgettext("block:file-upload", "Allowed file types"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "extensions", undefined)
                    ).placeholder(
                        pgettext(
                            "block:file-upload",
                            "Comma separated list, e.g. .png, .jpg"
                        )
                    ),
                ],
            },
            activated: isString(this.extensions),
        });

        this.editor.groups.options();
        this.editor.required(this.fileSlot);
        this.editor.visibility();
        this.editor.alias(this.fileSlot);
    }

    @conditions
    defineConditions(): void {
        each(
            [
                {
                    label: pgettext("block:file-upload", "File uploaded"),
                    icon: ICON_UPLOADED,
                    isUploaded: true,
                },
                {
                    label: pgettext("block:file-upload", "No file uploaded"),
                    icon: ICON_NOT_UPLOADED,
                    isUploaded: false,
                },
            ],
            (condition) => {
                this.conditions.template({
                    condition: FileUploadCondition,
                    label: condition.label,
                    icon: condition.icon,
                    props: {
                        slot: this.fileSlot,
                        isUploaded: condition.isUploaded,
                    },
                });
            }
        );
    }
}
